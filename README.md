# RPoD Gopher & Gemini Proxy

The RPoD Gopher & Gemini Proxy allows you to surf gopher and gemini protocols from your web browser. Place the index.php in a directory named gopher in the root of your web server.

The .htaccess works with apache to rewrite URLs so that (for example) https://leveck.us/1436.ninja is the same as https://leveck.us/index.php?url=1436.ninja

See this proxy in action at https://leveck.us/
