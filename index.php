<?PHP
/////////////////////////////////////////
// RPoD Gopher/Gemini Proxy Version 1.2
// (c) 2017-2020 Nathaniel Leveck
// gopher://1436.ninja
// gemini://1436.ninja/
// https://leveck.us
// leveck@leveck.us
// This code is released under the GPLv3
// See the LICENSE file
/////////////////////////////////////////
$myserver = "yourdomain.com";

$url="";$schm="";$host="";$port="";$path="";$reff="";$raw="";$query="";$sid=1;$flag=0;
if(isset($_REQUEST["url"])){
  $url=urldecode($_REQUEST["url"]);
}else{
  // Used by the gopher code, an alt way to get path
  if(isset($_REQUEST["h"]) && $_REQUEST["h"]!=""){
    $host = $_REQUEST["h"];
  }
  if(isset($_REQUEST["p"]) && $_REQUEST["p"]!=""){
    $port = $_REQUEST["p"];
  }
  if(isset($_REQUEST["r"]) && $_REQUEST["r"]!=""){
    $path = urldecode($_REQUEST["r"]);
  }
  if($path===""){
    $path="/1/";
  }
  if($query!=""){
    $path=$path."?$query";$path="/7/$path";
  }
}

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////START GEMINI PROTOCOL CODE//////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

if (strpos($url, "gemini/") === 0) {
  //Gemini Protocol Code
  //Nathaniel Leveck September 2020
  $gemini = true;
  $url = substr($url,7);
  $url = "http://" . str_replace("%3F","?",$url);
  $host = parse_url($url, PHP_URL_HOST);
  $port = parse_url($url, PHP_URL_PORT);
  $path = parse_url($url, PHP_URL_PATH);
  $pathinfo = pathinfo($path);
  $qq =  parse_url($url, PHP_URL_QUERY);
  $url = str_replace('http://', 'gemini://', $url);
  $host = $host;
  if ($port == ""){
    $port = 1965;
  }
  //secure connection to gemini server
  $cert = '/var/www/cert/cert.pem';
  $context = stream_context_create();
  stream_context_set_option($context, 'ssl', 'local_cert', $cert);
  stream_context_set_option($context, 'ssl', 'verify_peer', false);
  stream_context_set_option($context, 'ssl', 'verify_peer_name', false);
  stream_context_set_option($context, 'ssl', 'allow_self_signed', true);
  $socket = stream_socket_client('ssl://'.$host.':'.$port, $error, $errorString, 2,
  STREAM_CLIENT_CONNECT, $context);
  fwrite($socket, $url . "\r\n");
  $status = stream_get_line($socket,8024,"\r\n");
  if (strpos($status,"31") === 0){
    $redir = str_replace("gemini://","https://" . $myserver . "/gemini/",substr($status,3,strlen($status)));
    header('Location: '.$redir);
  }
  $content = stream_get_contents($socket);
  //check for binary files, send header, set flag
  switch(substr(strtolower($url),-3)){
    case "jpg":
    case "peg":
      header("Content-type:image/jpeg");
      $flag = 1;
      break;
    case "png":
      header("Content-type:image/png");
      $flag = 1;
      break;
    case "gif":
      header("Content-type:image/gif");
      $flag = 1;
      break;
    case "r.z":
    case ".gz":
      header("Content-type:application/x-tar");
      $flag = 1;
      break;
    case "bz2":
      header("Content-type:application/x-bzip2");
      $flag = 1;
      break;
    case "zip":
      header("Content-type:application/zip");
      $flag = 1;
      break;
    case "rar":
      header("Content-type:application/x-rar-compressed");
      $flag = 1;
      break;
    case "hqx":
      header("Content-type:application/mac-binhex");
      $flag = 1;
      break;
    case "pdf":
      header("Content-type:application/pdf");
      $flag = 1;
      break;
    case "doc":
    case "ocx":
      header("Content-type:application/msword");
      $flag = 1;
      break;
    case "mp3":
      header("Content-type:audio/mpeg3");
      $flag = 1;
      break;
    case "prc":
    case "pdb":
      header("Content-type:application/vnd.palm");
      $flag = 1;
      break;
  }

  if ($flag == 1){
    print $content;
    exit;
  }
  fclose($socket);
  $array = preg_split("/\r\n|\n|\r/", $content);
  $content = '<pre>'; 
  foreach($array as $item) {
    $item = trim($item);
    $item = preg_replace("/^###/", "</pre><h3>", $item);
    $item = preg_replace("/^##/", "</pre><h2>", $item);
    $item = preg_replace("/^#/", "</pre><h1>", $item);
    $item = preg_replace("/^\*/", "</pre><li>", $item);
    if ( strpos($item,'</pre><h3>') === 0 ){
      $item = $item . '</h3><pre>';
    } 
    if ( strpos($item,'</pre><h2>') === 0 ){
      $item = $item . '</h2><pre>';
    } 
    if ( strpos($item,'</pre><h1>') === 0 ){
      $item = $item . '</h1><pre>';
    } 
    if ( strpos($item,'</pre><li>') === 0 ){
      $item = $item . '</li><pre>';
    } 
    if ( preg_match("/^=>/", $item) ){
      $item = trim(preg_replace("/=>/","", $item));
      if (preg_match("/^\/\//", $item)){$item = "gemini:" . $item;}
      if (preg_match("/^gemini:\/\//", $item) || preg_match("/^http/", $item) || preg_match("/^gopher:\/\//", $item)){
        //we're good
      }elseif (preg_match("/^\//", $item)){
        $item = "gemini://" . $host . trim($item);
      }elseif (preg_match("/^\.\s/", $item)){
        $item = "gemini://" . $host . $pathinfo['dirname'] . preg_replace("/^\./","/",trim($item));
      }else{
        if (strrpos($url,"/") == strlen($url) -1){
          $item = $url . trim($item);
        }else{
          $item = "gemini://" . $host . str_replace("//", "/", $pathinfo['dirname'] . "/" . trim($item));
        }
      }
      $item = trim($item);
      $urlreg = '@(http|https|ftp|ftps|gopher|gemini|telnet)://[^\s]*@';
      $item = str_replace(" \"","\"",preg_replace($urlreg, '<a href="$0" title="$0">[Link]', $item));
      $item = preg_replace("/$/", "</a>", $item);
      if (preg_match("/\"\>\[Link\]\</", $item) >= 1){ // && strpos($item,"http") === false){
        $item = str_replace('[Link]',str_replace('title="',"",substr($item,strpos($item,'title'))),$item);
        $item = str_replace('">[Link]','',$item);
        $item = str_replace("gemini://","https://" . $myserver . "/gemini/",$item);
        $item = str_replace('>https://' . $myserver . '/gemini/','>gemini://',$item);
      }else{
        $item = str_replace('[Link]','',$item);
        $item = str_replace("gemini://","https://" . $myserver . "/gemini/",$item);
      }
    }
    if (trim($item) !== "```"){
      $content = $content . str_replace("> ",">",trim($item)) . "\n";
    }
  }
  $content = $content . "</pre>";

//////////////////////////////////////////////////////////////////////////////////////
///////////////////////////END GEMINI, START GOPHER///////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

}else{
  //Gopher Protocol Code
  //Nathaniel Leveck 2017-2020
  if(isset($_REQUEST["raw"]) && $_REQUEST["raw"]!=""){$raw = $_REQUEST["raw"];}
  if(isset($_REQUEST["ref"]) && $_REQUEST["ref"]!=""){$reff = $_REQUEST["ref"];}
  //to fool php parse_url below
  $url = "http://" . str_replace("%3F","?",$url);
  $host = parse_url($url, PHP_URL_HOST);
  $port = parse_url($url, PHP_URL_PORT);
  $path = parse_url($url, PHP_URL_PATH);
  $qq =  parse_url($url, PHP_URL_QUERY);
  if(isset($_REQUEST["q"]) && $_REQUEST["q"]!=""){$qq = $_REQUEST["q"];}
  if($qq != ""){$qq="?".$qq;}
  if($port == ""){$port="70";}
  if($host == ""){$host="1436.ninja";$port="70";$path="/1/";}
  if($schm==""){$schm="gopher";}
  if($qq != ""){$url = $schm."://".$host.":".$port.$path.$qq.$q;}else{$url = $schm."://".$host.":".$port.$path;}
  $ch = curl_init($url);
  if($path[1] === "I" || $path[1] === "G" || $path[1] === "4" || $path[1] === "5" || $path[1] === "9" || $path[1] === "s"){
  //display image or dl bin files
  curl_setopt($ch, CURLOPT_HEADER, 1);
  curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
  switch(substr(strtolower($path),-3)){
    case "jpg":
      header("Content-type:image/jpeg");
      break;
    case "png":
      header("Content-type:image/png");
      break;
    case "peg":
      header("Content-type:image/jpeg");
      break;
    case "gif":
      header("Content-type:image/gif");
      break;
    case "r.z":
    case ".gz":
      header("Content-type:application/x-tar");
      break;
    case "bz2":
      header("Content-type:application/x-bzip2");
      break;
    case "zip":
      header("Content-type:application/zip");
      break;
    case "rar":
      header("Content-type:application/x-rar-compressed");
      break;
    case "hqx":
      header("Content-type:application/mac-binhex");
      break;
    case "pdf":
      header("Content-type:application/pdf");
      break;
    case "doc":
    case "ocx":
      header("Content-type:application/msword");
      break;
    case "mp3":
      header("Content-type:audio/mpeg3");
      break;
    case "prc":
    case "pdb":
      header("Content-type:application/vnd.palm");
      break;
    }
  $rawdata=curl_exec($ch);
  curl_close ($ch);
  exit;
  }
  if(substr($url, strlen($url)-5, 5) == ".post" || substr($url, strlen($url)-3, 3) == ".md") {
    $content = str_replace("gopher://", "https://" . $myserver ."/", shell_exec("curl -s " . $url . " | pandoc -f markdown+pipe_tables+footnotes -t html"));
    $md = true;
  }
  if($md != true){
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $content = curl_exec($ch);
    if(strpos($path,"/0") === 0 || strpos($path,"/h") === 0){$raw="on";}
    if($raw != 'on'){
      $array = preg_split("/\r\n|\n|\r/", $content);
      $content="";
      foreach($array as $item) {
        $item = trim($item);
        $typ = $item[0];
        $flg = 0;
        //goes against the rfc, but gophernicus doesn't put 0
        //in front of text files in auto-generated directory listings
        //when oldass txt files use file extentions belonging to
        //assumed other types...
        if($typ === '0' || $typ === 'M' || $typ === 'm' || $typ === 'C' || $typ === 'c'){
          //text
          $tmp = preg_split("/\t/", $item);
          $zero = substr($tmp[0],1);
          $thisline = "<tt><img src='/images/text.png' class='item-img'>&nbsp;<a href='/".$tmp[2].":".$tmp[3]."/0".str_replace("?","%3F",$tmp[1])."'>".trim(rtrim(trim($zero),"-"))."</a></tt><br />\n";
          $flg = 1;
        }
        if($typ === '1' && $flg === 0 && strpos($item,"\t") != 0){
          //directory
          $tmp = preg_split("/\t/", $item);
          $zero = substr($tmp[0],1);
          $thisline = "<tt><img src='/images/directory.png' class='item-img'>&nbsp;<a href='/".$tmp[2].":".$tmp[3]."/1".str_replace("?","%3F",$tmp[1])."'>".trim(rtrim(trim($zero),"-"))."</a></tt><br />\n";
          $flg = 1;
        }elseif($typ === 'i' && $flg === 0 && strpos($item,"\t") === 0){
          $typ=0;
          $flg=0;
        }
        if($typ === '4' && $flg === 0 && strpos($item,"\t") != 0 || $typ === 'd' && $flg === 0 && strpos($item,"\t") != 0){
          //Archives & binhex
          $tmp = preg_split("/\t/", $item);
          $zero = substr($tmp[0],1);
          $thisline = "<tt><img src='/images/binary.png' class='item-img'>&nbsp;<a href='/".$tmp[2].":".$tmp[3]."/5".$tmp[1]."'>$zero</a></tt><br />\n";
          $flg = 1;
        }elseif($typ === 'i' && $flg === 0 && strpos($item,"\t") === 0){
          $typ=0;
          $flg=0;
        }
        if($typ === '7' && $flg === 0 && strpos($item,"\t") != 0){
          //search (input)
          $tmp = preg_split("/\t/", $item);
          $zero = substr($tmp[0],1);
          $thisline = "<tt><img src='/images/query.png' class='item-img'>&nbsp;<a onclick='document.getElementById(".chr(34)."s$sid".chr(34).").style.display=".chr(34)."block".chr(34).";'><span id='y".$sid."' style='cursor:pointer;' title='Click here to enable input field'>$zero</span></a><div id='s".$sid."' style='display:none;'><form id='srch' action='/index.php' method='get'><input type='hidden' name='url' id='url' value='".$tmp[2].":".$tmp[3]."/7".$tmp[1]."'><input type='text' name='q' id='q'><input type='submit' value='SUBMIT'></form></div></tt><br>\n";
          $flg = 1;
          $sid=$sid+1;
        }elseif($typ === 'i' && $flg === 0 && strpos($item,"\t") === 0){
          $typ=0;
          $flg=0;
        }
        if($typ === '8' && $flg === 0 && strpos($item,"\t") != 0){
          //telnet
          $tmp = preg_split("/\t/", $item);
          $zero = substr($tmp[0],1);
          $thisline = "<tt><img src='/images/telnet.png' class='item-img'>&nbsp;<a href='telnet://".$tmp[2].":".$tmp[3]."/5".$tmp[1]."'>$zero</a></tt><br />\n";
          $flg = 1;
        }
        if($typ === '9' && $flg === 0 && strpos($item,"\t") != 0 || $typ === '5' && $flg === 0 && strpos($item,"\t") != 0 || $typ === 's' && $flg === 0 && strpos($item,"\t") != 0 || $typ === 'P' && $flg === 0 && strpos($item,"\t") != 0){
          //Binary
          $tmp = preg_split("/\t/", $item);
          $zero = substr($tmp[0],1);
          if($typ === 's'){
            $thisline = "<tt><img src='/images/music.png' class='item-img'>&nbsp;<a href='/".$tmp[2].":".$tmp[3]."/9".$tmp[1]."'>$zero</a></tt><br>\n";
          }
          if($typ === '9' || $typ === '5'){
            $thisline = "<tt><img src='/images/binary.png' class='item-img'>&nbsp;<a href='/".$tmp[2].":".$tmp[3]."/9".$tmp[1]."'>$zero</a></tt><br>\n";
          }
          if($typ === 'P'){
            $thisline = "<tt><img src='/images/pdf.png' class='item-img'>&nbsp;<a href='/".$tmp[2].":".$tmp[3]."/9".$tmp[1]."'>$zero</a></tt><br>\n";}
            $flg = 1;
          }
        if($typ === 'h' && $flg === 0 && strpos($item,"\t") != 0 || $typ === 'w' && $flg === 0 && strpos($item,"\t") != 0){
          //html
          if(strpos($item, "URL")){
            $item = str_replace("gemini://","https://" . $myserver . "/gemini/",$item);
            $tmp = preg_split("/\t/", $item);
            $zero = substr($tmp[0],1);
            $thisline = "<tt><img src='/images/html.png' class='item-img'>&nbsp;<a href='".substr($tmp[1],4)."'>".trim(rtrim(trim($zero),"-"))."</a></tt><br />\n";
            $flg = 1;
          }else{
            //type h but as a file via gopher, no URL:
            $tmp = preg_split("/\t/", $item);
            $zero = substr($tmp[0],1);
            $thisline = "<tt><img src='/images/html.png' class='item-img'>&nbsp;<a href='/".$tmp[2].":".$tmp[3]."/h".str_replace("?","%3F",$tmp[1])."'>".trim(rtrim(trim($zero),"-"))."</a></tt><br />\n";
            $flg = 1;
          }
        }elseif($typ === 'i' && $flg === 0 && strpos($item,"\t") == 0){
          $typ=0;
          $flg=0;
        }
        if($typ === 'i' && $flg === 0){
          //text
          $thisline = "<tt>".htmlspecialchars(str_replace("<script", "script", substr($item,1,strpos($item,"\t"))))."</tt><br>\n";
          $flg = 1;
        }
        if($typ === 'I' && $flg === 0 && strpos($item,"\t") || $typ === 'g' && $flg === 0 && strpos($item,"\t") || $typ === 'p' && $flg === 0 && strpos($item,"\t") != 0){
          //image
          $tmp = preg_split("/\t/", $item);
          $zero = substr($tmp[0],1);
          $thisline = "<tt><img src='/images/image.png' class='item-img'>&nbsp;<a href='/".$tmp[2].":".$tmp[3]."/I".$tmp[1]."'>".trim(rtrim(trim($zero),"-"))."</a></tt><br />\n";
          $flg = 1;
        }elseif($typ === 'i' && $flg === 0 && strpos($item,"\t") === 0){
          $typ=0;
          $flg=0;
        }
        if($flg === 0){
          //plaintext
          $thisline = "<tt>".wordwrap(preg_replace("/^i/","",$item),82,"</tt>\n<tt>")."</tt><br />\n";
          $flg = 1;
        }
        $content .= $thisline;
        $thisline = "";
      }
    }elseif(strpos($path,"/h") === 0){
      $content = "<!-- Proxying an html document -->\n" . $content;
    }else{
      $content="<!-- Proxying a text document -->\n<pre>". htmlspecialchars($content) ."</pre>";
    }
  }
}
?>
<!DOCTYPE html>
<!--
===========================================================
       ____  ____       ____
      |  _ \|  _ \ ___ |  _ \
      | |_) | |_) / _ \| | | | Raspberry Pi of Death
      |  _ <|  __/ (_) | |_| | Gopher/Gemini Proxy
      |_| \_\_|   \___/|____/

<?PHP 
if ($gemini != true){
?>This content is hosted via gopher, on port <?PHP 
  print $port . ".\n";
}else{
?>This content is hosted via gemini, on port <?PHP 
  print $port . ".\n";
}
?>
RPoD Gopher/Gemini Proxy allows you to browse these 
protocols from your web browser. This particular instance 
displays RPoD (gopher://1436.ninja) by default. To host 
your own instance:

git clone https://gitlab.com/leveck/rpod-gopher-proxy

If you do this make certain your robots.txt looks like 
this:

  User-agent: *
  Disallow: /

Keep gopher and gemini content off of search engines!!

<?PHP
print "URL requested: " . $url . "\n"; 
?>

============================================================
-->
<html lang="en">
<head>
<title>RPoD Gopher Proxy</title>
<meta id="Viewport" name="viewport" content="width=device-width, initial-scale=1.0">
<style>
@media (min-width: 10px) {body {width:100%;background-color:#000000;color:#bbbbbb;font-family:'monospace';font-weight:normal;font-size:2vw}a {text-decoration:none;color:#666666;} input {border:0;}tt {white-space: pre-wrap;} pre {white-space:pre-wrap;margin:0;padding:0;} .item-img{width:1.5vw;} td,th{border: 1px solid #383838;margin:3px;padding:5px} li{margin-left: 2em;}}
@media (min-width: 1024px) {body {width:99%;background-color:#000000;color:#bbbbbb;font-size:14pt;font-family:'monospace';font-weight:normal;}a {text-decoration:none;color:#666666;} input {border:0;}tt {white-space: pre-wrap;} pre {white-space:pre-wrap;margin:0;padding:0;} #content {padding-top: 2em;width:46em;text-align:left;margin:auto;font-weight:normal;} #parent{width:100%;position: relative;} .item-img{width: .75em;vertical-align: middle;} td,th{border: 1px solid #383838;margin:3px;padding:5px} li{margin-left: 2em;}}
</style>
</head>
<body>
<div id="parent"><div id="content"><?php
$content = str_replace("gopher://","https://" . $myserver . "/",$content);
print "$content";
?></div></div>
</body>
</html>
